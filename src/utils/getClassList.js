
const getClassList = ({
  error,
  required
}) => {
  const classList = []
  if (error) classList.push('error')
  if (required) classList.push('required')
  return classList
}

export default getClassList
