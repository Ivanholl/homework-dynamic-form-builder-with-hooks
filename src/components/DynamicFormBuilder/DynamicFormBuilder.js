import React, { useState, useEffect } from 'react'
import TextInput from '../inputTypes/TextInput'
import SelectInput from '../inputTypes/SelectInput'
import CheckboxInput from '../inputTypes/CheckboxInput'

import './DynamicFormBuilder.scss'

const DynamicFormBuilder = ({
  formFields: { 
    fields = [] 
  } = {}
}) => {
  const [formFields, setFormFields] = useState([])

  useEffect(() => {
    if (fields?.length) {
      const values = {}
      fields.forEach((field) => {
        values[field.id] = ''
      })
      setFormFields(fields)
    }
  }, [fields])

  // Check and Set Error if any of the formFields that is required and has no value
  const validateForm = () => {
    let hasErr = true
    const newFields = formFields.map((field) => {
      if (field.required && (!field.value && !field.hidden)) {
        hasErr = false
        return { ...field, error: true }
      }
      return { ...field, error: false }
    })
    setFormFields(newFields)
    return hasErr
  }

  const onSubmit = (event) => {
    event.preventDefault()
    const isValid = validateForm()
    if (!isValid) return
    // would like to render properly
    const result = formFields.map(({id, value}) => `${id}-${value}`).toString().split(',').join('\r\n')
    alert(`A form was submitted:\r\n${result}`)
  }

  const onChange = (event, id) => {
    const { target: { value, checked } = {} } = event

    const newFields = formFields.map((field) => {
      if (field.id === id) field.value = value || checked || ''
      return field
    })
    setFormFields(newFields)
  }

  // If a checkbox has clickToEnable: 'anotherFieldId'
  // Find a field by Id and change it's hidden value based on checkbox value
  const showHiddenField = (hiddenFieldId, isVisible) => {
    const fieldToShow = formFields.find((field) => field.id === hiddenFieldId)
    fieldToShow.hidden = !isVisible

    setFormFields({
      ...formFields,
      fieldToShow
    })
  }

  const getInputType = (inputData) => {
    const { id, hidden } = inputData
    if (hidden) return

    switch (inputData.type) {
      case 'text':
        return (
          <TextInput
            key={id}
            {...inputData} //pass all Meta via spread             
            handleOnChange={(event) => onChange(event, id)}
          />
        )
      case 'select':
        return (
          <SelectInput
            key={id}
            {...inputData} //pass all Meta via spread
            handleOnChange={(event) => onChange(event, id)}
          />
        )
      case 'checkbox':
        return (
          <CheckboxInput
            key={id}
            {...inputData} //pass all Meta via spread
            showHiddenField={showHiddenField}
            handleOnChange={(event, id) => onChange(event, id)}
          />
        )
      default:
        return null
    }
  }

  if (!formFields) return null
  return (
    <form onSubmit={(event) => { 
      validateForm()
      onSubmit(event) 
    }}>
      {formFields.map((formField) => getInputType(formField))}
      <button type="submit">Submit</button>
    </form>
  )
}

export default DynamicFormBuilder
