import React from 'react'
import getClassList from '../../utils/getClassList'

const CheckboxInput = (props) => {
  const {
    id,
    required = false,
    error = false,
    label = '',
    type = 'checkbox',
    handleOnChange = () => {},
    clickToEnable = '',
    showHiddenField
  } = props
  const classList = getClassList({ required, error })

  const handleChange = (event) => {
    const { target: { checked } = {} } = event
    if (clickToEnable && showHiddenField) showHiddenField(clickToEnable, checked)
    handleOnChange(event, id)
  }

  return (
    <>
      <label id={id} className={classList}>
        <p>{label}</p>
        <input type={type} onChange={handleChange} {...props}/>
      </label>
    </>
  )
}

export default CheckboxInput
