import React from 'react'
import getClassList from '../../utils/getClassList'

function TextInput(props) {
  const {
    id,
    required = false,
    label = '',
    error = false,
    placeholder = '',
    type = 'text',
    handleOnChange = () => {}
  } = props
  const classList = getClassList({ required, error })

  return (
    <label id={id} className={classList}>
      <p>{label}</p>
      <input type={type} placeholder={placeholder} onChange={handleOnChange} {...props}/>
    </label>
  )
}

export default TextInput
