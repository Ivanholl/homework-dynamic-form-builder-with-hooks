import React from 'react'
import getClassList from '../../utils/getClassList'

function SelectInput(props) {
  const {
    id,
    required = false,
    label = '',
    error = false,
    placeholder = '',
    options = [],
    handleOnChange = () => {}
  } = props
  const classList = getClassList({ required, error })

  return (
    <label id={id} className={classList}>
      <p>{label}</p>
      <select onChange={handleOnChange} {...props}>
        <option value="">{placeholder}</option>
        {options.map((option) => {
          return (
            <option key={option?.label} value={option?.label}>
              {option?.label}
            </option>
          )
        })}
      </select>
    </label>
  )
}

export default SelectInput
