import React from 'react'
import formJSON from './formData.json'
import DynamicFormBuilder from './components/DynamicFormBuilder/DynamicFormBuilder'

import './App.scss'

function App() {
  const tempFormJSON = formJSON || []
  return (
    <div className="container">
      <h1>Dynamic Form Builder</h1>
      {tempFormJSON.map((formFields, i) => (
        <DynamicFormBuilder key={i} formFields={formFields} />
      ))}
    </div>
  )
}

export default App
